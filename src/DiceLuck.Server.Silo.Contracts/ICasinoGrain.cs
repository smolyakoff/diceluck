﻿using DiceLuck.Core.Contracts;
using Orleans;

namespace DiceLuck.Server.Silo.Contracts
{
    public interface ICasinoGrain : IGrainWithIntegerKey, ICasino
    {
    }
}
