﻿using System.Threading.Tasks;

namespace DiceLuck.Framework.Contracts.Persistence
{
    public interface IStateHandle<TKey, out TState> where TState : class, IState<TKey>
    {
        TState State { get; }

        Task Persist();

        Task Restore();

        Task Clear();
    }
}
