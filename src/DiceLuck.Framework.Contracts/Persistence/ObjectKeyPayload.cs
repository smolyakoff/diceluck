﻿namespace DiceLuck.Framework.Contracts.Persistence
{
    public class ObjectKeyPayload
    {
        public object Key { get; set; }

        public string Type { get; set; }
    }
}