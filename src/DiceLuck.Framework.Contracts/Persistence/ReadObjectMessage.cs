﻿using MediatR;

namespace DiceLuck.Framework.Contracts.Persistence
{
    public class ReadObjectMessage : IPersistenceMessage<ObjectKeyPayload>, IAsyncRequest<object>
    {
        public ObjectKeyPayload Payload { get; set; }
    }
}