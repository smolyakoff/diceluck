﻿namespace DiceLuck.Framework.Contracts.Persistence
{
    public class ObjectKeyValuePayload : ObjectKeyPayload
    {
        public object Value { get; set; }
    }
}