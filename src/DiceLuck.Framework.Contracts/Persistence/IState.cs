﻿namespace DiceLuck.Framework.Contracts.Persistence
{
    public interface IState<out TKey>
    {
        TKey Key { get; }
    }
}
