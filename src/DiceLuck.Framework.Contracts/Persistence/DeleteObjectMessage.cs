﻿using MediatR;

namespace DiceLuck.Framework.Contracts.Persistence
{
    public class DeleteObjectMessage : IPersistenceMessage<ObjectKeyPayload>, IAsyncRequest
    {
        public ObjectKeyPayload Payload { get; set; }
    }
}