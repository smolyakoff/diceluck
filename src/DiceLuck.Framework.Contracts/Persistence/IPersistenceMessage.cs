﻿using DiceLuck.Framework.Contracts.Messaging;

namespace DiceLuck.Framework.Contracts.Persistence
{
    public interface IPersistenceMessage<out T> : IPayloadContainer<T> where T : ObjectKeyPayload
    {
    }
}
