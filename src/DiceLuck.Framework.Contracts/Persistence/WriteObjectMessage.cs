﻿using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Framework.Contracts.Persistence
{
    public class WriteObjectMessage : IPayloadContainer<ObjectKeyValuePayload>, IAsyncRequest
    {
        public ObjectKeyValuePayload Payload { get; set; }
    }
}
