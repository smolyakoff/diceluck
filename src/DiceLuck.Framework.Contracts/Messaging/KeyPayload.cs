﻿namespace DiceLuck.Framework.Contracts.Messaging
{
    public class KeyPayload<T>
    {
        public KeyPayload(T key)
        {
            Key = key;
        } 

        public T Key { get; }
    }
}
