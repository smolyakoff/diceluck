﻿namespace DiceLuck.Framework.Contracts.Messaging
{
    public interface IMetadataContainer<T>
    {
        T Metadata { get; set; }
    }
}
