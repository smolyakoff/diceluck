﻿namespace DiceLuck.Framework.Contracts.Messaging
{
    public class Request
    {
        public string Type { get; set; }

        public object Payload { get; set; }

        public object Meta { get; set; }
    }
}