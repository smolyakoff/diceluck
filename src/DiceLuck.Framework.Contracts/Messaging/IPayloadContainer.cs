﻿namespace DiceLuck.Framework.Contracts.Messaging
{
    public interface IPayloadContainer<out T>
    {
        T Payload { get; }
    }
}
