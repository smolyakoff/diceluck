﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using DiceLuck.Core.Contracts;
using DiceLuck.Framework.Contracts.Persistence;

namespace DiceLuck.Core
{
    public class Casino : ICasino
    {
        private readonly IStateHandle<long, ICasinoState> _stateHandler;

        public Casino(IStateHandle<long, ICasinoState> stateHandler)
        {
            _stateHandler = stateHandler;
        }

        public Task<DiceRollIncomePayload> Handle(CalculateIncomeMessage message)
        {
            Console.WriteLine($"Casino Process Id: {Process.GetCurrentProcess().Id}");
            var income = _stateHandler.State.IncomeTable[message.Payload.Dice1 + message.Payload.Dice2];
            var result = new DiceRollIncomePayload {Income = income};
            return Task.FromResult(result);
        }

        public Task<JackpotVerificationResultPayload> Handle(VerifyJackpotMessage message)
        {
            Console.WriteLine($"Casino Process Id: {Process.GetCurrentProcess().Id}");
            var isJackpot = message.Payload.PlayerName == _stateHandler.State.JackpotWinnerName && 
                message.Payload.Game.Rolls == _stateHandler.State.JackpotNumber;
            return Task.FromResult(new JackpotVerificationResultPayload {IsJackpot = isJackpot});
        }
    }
}
