﻿using System;
using System.Threading.Tasks;
using DiceLuck.Core.Contracts;
using DiceLuck.Framework.Contracts.Persistence;
using MediatR;

namespace DiceLuck.Core
{
    public class Player : IPlayer
    {
        private readonly IMediator _mediator;
        private readonly IStateHandle<Guid, IPlayerState> _stateHandle;

        public Player(IStateHandle<Guid, IPlayerState> stateHandle, IMediator mediator)
        {
            _stateHandle = stateHandle;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdatePlayerProfileMessage message)
        {
            _stateHandle.State.Name = message.Payload.Name;
            await _stateHandle.Persist();
            return Unit.Value;
        }

        public Task<PlayerProfilePayload> Handle(GetPlayerProfileMessage message)
        {
            var payload = new PlayerProfilePayload
            {
                Key = _stateHandle.State.Key,
                Name = _stateHandle.State.Name
            };
            return Task.FromResult(payload);
        }
    }
}
