﻿using System;
using System.Threading.Tasks;
using DiceLuck.Core.Contracts;
using DiceLuck.Framework.Contracts.Persistence;
using MediatR;

namespace DiceLuck.Core
{
    public class Game : IGame
    {
        private readonly IMediator _mediator;
        private readonly Random _random;

        private PlayerProfilePayload _player;

        private readonly IStateHandle<Guid, IGameState> _stateHandle; 

        public Game(IStateHandle<Guid, IGameState> stateHandle, IMediator mediator)
        {
            _stateHandle = stateHandle;
            _mediator = mediator;
            _random = new Random();
        }

        public async Task<Unit> Handle(StartGameMessage message)
        {
            var profileMessage = new GetPlayerProfileMessage
            {
                Metadata = message.Metadata,
                Payload = new Contracts.KeyPayload<Guid>(message.Metadata.User.UserId)
            };
            _player = await _mediator.SendAsync(profileMessage);
            return Unit.Value;
        }

        public async Task<DiceRollOutcomePayload> Handle(RollDiceMessage message)
        {
            var dice1 = _random.Next(1, 7);
            var dice2 = _random.Next(1, 7);
            _stateHandle.State.Rolls++;
            var incomeMessage = new CalculateIncomeMessage
            {
                Payload = new DiceRollResultPayload {Dice1 = dice1, Dice2 = dice2}
            };
            var incomeResponse = await _mediator.SendAsync(incomeMessage);
            var jackpotMessage = new VerifyJackpotMessage
            {
                Payload = new JackpotVerificationPayload
                {
                    DiceRollResult = new DiceRollResultPayload {Dice1 = dice1, Dice2 = dice2},
                    Game = new GameStatePayload {Income = _stateHandle.State.Income, Rolls = _stateHandle.State.Rolls},
                    PlayerName = _player.Name
                }
            };
            var jackpotResponse = await _mediator.SendAsync(jackpotMessage);
            var outcome = new DiceRollOutcomePayload
            {
                FirstDice = dice1,
                SecondDice = dice2,
                Income = incomeResponse.Income,
                IsJackpot = jackpotResponse.IsJackpot
            };
            return outcome;
        }
    }
}
