﻿using System;
using DiceLuck.Core;
using Orleans;

namespace DiceLuck.Server.Silo
{
    public class PlayerGrainState : GrainState, IPlayerState
    {
        public Guid Key { get; set; }

        public string Name { get; set; }
    }
}