﻿using System.Reflection;
using System.Threading.Tasks;
using DiceLuck.Framework.Contracts.Persistence;
using Orleans;

namespace DiceLuck.Server.Silo
{
    public class GrainStateHandle<TKey, TState> : IStateHandle<TKey, TState> where TState : GrainState, IState<TKey>
    {
        private readonly Grain<TState> _grain;
        private readonly PropertyInfo _state;
        private readonly MethodInfo _write;
        private readonly MethodInfo _restore;
        private readonly MethodInfo _clear;

        public GrainStateHandle(Grain<TState> grain)
        {
            _grain = grain;
            _state = grain.GetType().GetProperty("State", BindingFlags.NonPublic | BindingFlags.Instance);
            _write = grain.GetType().GetMethod("WriteStateAsync", BindingFlags.Instance | BindingFlags.NonPublic);
            _restore = grain.GetType().GetMethod("ReadStateAsync", BindingFlags.Instance | BindingFlags.NonPublic);
            _clear = grain.GetType().GetMethod("ClearStateAsync", BindingFlags.Instance | BindingFlags.NonPublic);
        }

        public TState State => (TState)_state.GetValue(_grain);

        public Task Persist()
        {
            return (Task) _write.Invoke(_grain, new object[] {});
        }

        public Task Restore()
        {
            return (Task) _restore.Invoke(_grain, new object[] {});
        }

        public Task Clear()
        {
            return (Task)_clear.Invoke(_grain, new object[] {});
        }
    }
}
