﻿using System.Threading.Tasks;
using DiceLuck.Core;
using DiceLuck.Core.Contracts;
using DiceLuck.Server.Silo.Contracts;
using Orleans;

namespace DiceLuck.Server.Silo
{
    public class CasinoGrain : Grain<CasinoGrainState>, ICasinoGrain
    {
        private Casino _casino;

        public override async Task OnActivateAsync()
        {
            await base.OnActivateAsync();
            var stateHandle = new GrainStateHandle<long, CasinoGrainState>(this);
            _casino = new Casino(stateHandle);
        }

        public Task<DiceRollIncomePayload> Handle(CalculateIncomeMessage message)
        {
            return _casino.Handle(message);
        }

        public Task<JackpotVerificationResultPayload> Handle(VerifyJackpotMessage message)
        {
            return _casino.Handle(message);
        }
    }
}
