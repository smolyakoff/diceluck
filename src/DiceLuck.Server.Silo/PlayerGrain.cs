﻿using System;
using System.Threading.Tasks;
using DiceLuck.Core;
using DiceLuck.Core.Contracts;
using DiceLuck.Server.Silo.Contracts;
using MediatR;
using Orleans;

namespace DiceLuck.Server.Silo
{
    public class PlayerGrain : Grain<PlayerGrainState>, IPlayerGrain
    {
        private readonly IMediator _mediator;
        private Player _player;

        public PlayerGrain(IMediator mediator)
        {
            _mediator = mediator;
        }

        public override async Task OnActivateAsync()
        {
            await base.OnActivateAsync();
            State.Key = this.GetPrimaryKey();
            var stateHandle = new GrainStateHandle<Guid, PlayerGrainState>(this);
            _player = new Player(stateHandle, _mediator);
        }

        public Task<PlayerProfilePayload> Handle(GetPlayerProfileMessage message)
        {
            return _player.Handle(message);
        }

        public Task<Unit> Handle(UpdatePlayerProfileMessage message)
        {
            return _player.Handle(message);
        }
    }
}