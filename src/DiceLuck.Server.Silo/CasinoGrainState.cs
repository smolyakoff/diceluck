﻿using System.Collections.Generic;
using DiceLuck.Core.Contracts;
using Orleans;

namespace DiceLuck.Server.Silo
{
    public class CasinoGrainState : GrainState, ICasinoState
    {
        public long Key => 0;

        public CasinoGrainState()
        {
            JackpotWinnerName = "jocker";
            JackpotNumber = 2;
        }

        public string JackpotWinnerName { get; set; }
        public int JackpotNumber { get; set; }
        public Dictionary<int, double> IncomeTable { get; set; }
    }
}
