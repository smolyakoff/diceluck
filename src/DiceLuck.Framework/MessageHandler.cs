﻿using System.Threading.Tasks;
using DiceLuck.Core;
using DiceLuck.Core.Contracts;
using DiceLuck.Server.Silo.Contracts;
using MediatR;
using Orleans;

namespace DiceLuck.Server.Api
{
    public class MessageHandler : ICasino, IPlayer
    {
        public Task<DiceRollIncomePayload> Handle(CalculateIncomeMessage message)
        {
            return GrainClient.GrainFactory.GetGrain<ICasinoGrain>(default(long)).Handle(message);
        }

        public Task<JackpotVerificationResultPayload> Handle(VerifyJackpotMessage message)
        {
            return GrainClient.GrainFactory.GetGrain<ICasinoGrain>(default(long)).Handle(message);
        }

        public Task<PlayerProfilePayload> Handle(GetPlayerProfileMessage message)
        {
            return GrainClient.GrainFactory.GetGrain<IPlayerGrain>(message.Payload.Key).Handle(message);
        }

        public Task<Unit> Handle(UpdatePlayerProfileMessage message)
        {
            return GrainClient.GrainFactory.GetGrain<IPlayerGrain>(message.Payload.Key).Handle(message);
        }
    }
}
