﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using DiceLuck.Client;
using DiceLuck.Core.Contracts;
using MediatR;

namespace DiceLuck.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Delay(15000).Wait();
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ClientModule());
            var container = builder.Build();
            var mediator = container.Resolve<IMediator>();

            var id = Guid.NewGuid();
            Console.WriteLine("name?");
            var name = Console.ReadLine();
            var profileMessage = new UpdatePlayerProfileMessage
            {
                Payload = new PlayerProfilePayload
                {
                    Key = id,
                    Name = name
                }
            };
            mediator.SendAsync(profileMessage).Wait();

            var startGameMessage = new StartGameMessage
            {
                Metadata = new Metadata { User = new UserMetadata { UserId = id } }
            };
            mediator.SendAsync(startGameMessage).Wait();

            var rollMessage = new RollDiceMessage
            {
                Metadata = new Metadata {User = new UserMetadata {UserId = id}}
            };

            var total = 0d;
            Console.WriteLine("Type anything to roll");
            while (Console.ReadLine() != "exit")
            {
                var result = mediator.SendAsync(rollMessage).Result;
                total += result.Income;
                Console.WriteLine($"D1: {result.FirstDice} D2: {result.SecondDice}, Income: {result.Income}, Jackpot: {result.IsJackpot} Total: {total}");
            }

        }
    }
}
