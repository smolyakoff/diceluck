﻿namespace DiceLuck.Core.Contracts
{
    public class Metadata
    {
        public UserMetadata User { get; set; }

        public ClientMetadata Client { get; set; }
    }
}