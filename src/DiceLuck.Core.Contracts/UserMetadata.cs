﻿using System;

namespace DiceLuck.Core.Contracts
{
    public class UserMetadata
    {
        public Guid UserId { get; set; }
    }
}