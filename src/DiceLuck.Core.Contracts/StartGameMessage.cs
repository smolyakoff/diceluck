﻿using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core.Contracts
{
    public class StartGameMessage : IMetadataContainer<Metadata>, IAsyncRequest
    {
        public Metadata Metadata { get; set; }
    }
}
