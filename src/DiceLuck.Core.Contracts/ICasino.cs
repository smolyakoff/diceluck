﻿using MediatR;

namespace DiceLuck.Core.Contracts
{
    public interface ICasino : 
        IAsyncRequestHandler<CalculateIncomeMessage, DiceRollIncomePayload>,
        IAsyncRequestHandler<VerifyJackpotMessage, JackpotVerificationResultPayload>
    {
    }
}
