﻿using DiceLuck.Framework.Contracts;
using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core.Contracts
{
    public class RollDiceMessage : 
        IMetadataContainer<Metadata>, 
        IAsyncRequest<DiceRollOutcomePayload>
    {
        public Metadata Metadata { get; set; }
    }
}
