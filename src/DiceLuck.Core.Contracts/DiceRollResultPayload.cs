﻿namespace DiceLuck.Core
{
    public class DiceRollResultPayload
    {
        public int Dice1 { get; set; }

        public int Dice2 { get; set; }
    }
}