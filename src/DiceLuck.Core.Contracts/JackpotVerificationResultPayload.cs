﻿namespace DiceLuck.Core.Contracts
{
    public class JackpotVerificationResultPayload
    {
        public bool IsJackpot { get; set; }
    }
}