﻿using MediatR;

namespace DiceLuck.Core.Contracts
{
    public interface IGame :
        IAsyncRequestHandler<RollDiceMessage, DiceRollOutcomePayload>,
        IAsyncRequestHandler<StartGameMessage, Unit>
    {
    }
}
