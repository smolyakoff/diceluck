﻿using System;

namespace DiceLuck.Core.Contracts
{
    public class PlayerProfilePayload
    {
        public Guid Key { get; set; }

        public string Name { get; set; }
    }
}
