﻿using System;
using DiceLuck.Framework.Contracts.Persistence;

namespace DiceLuck.Core
{
    public interface IPlayerState : IState<Guid>
    {
        string Name { get; set; }
    }
}