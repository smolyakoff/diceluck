﻿namespace DiceLuck.Core.Contracts
{
    public class DiceRollOutcomePayload
    {
        public int FirstDice { get; set; }

        public int SecondDice { get; set; }

        public double Income { get; set; }

        public bool IsJackpot { get; set; }
    }
}
