﻿using System;
using DiceLuck.Framework.Contracts.Persistence;

namespace DiceLuck.Core
{
    public interface IGameState : IState<Guid>
    {
        int Rolls { get; set; }

        double Income { get; set; }
    }
}