﻿namespace DiceLuck.Core.Contracts
{
    public class KeyPayload<T>
    {
        public KeyPayload(T key)
        {
            Key = key;
        } 

        public T Key { get; }
    }
}
