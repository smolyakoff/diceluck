namespace DiceLuck.Core
{
    public class DiceRollIncomePayload
    {
        public double Income { get; set; }
    }
}