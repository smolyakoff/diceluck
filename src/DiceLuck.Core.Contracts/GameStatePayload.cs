﻿namespace DiceLuck.Core
{
    public class GameStatePayload
    {
        public int Rolls { get; set; }

        public double Income { get; set; }
    }
}