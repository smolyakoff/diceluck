﻿namespace DiceLuck.Core
{
    public class JackpotVerificationPayload
    {
        public DiceRollResultPayload DiceRollResult { get; set; }

        public string PlayerName { get; set; }

        public GameStatePayload Game { get; set; }
    }
}