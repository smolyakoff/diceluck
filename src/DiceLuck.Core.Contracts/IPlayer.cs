﻿using MediatR;

namespace DiceLuck.Core.Contracts
{
    public interface IPlayer : 
        IAsyncRequestHandler<GetPlayerProfileMessage, PlayerProfilePayload>,
        IAsyncRequestHandler<UpdatePlayerProfileMessage, Unit>
    {
    }
}
