﻿using System.Collections.Generic;
using DiceLuck.Framework.Contracts.Persistence;

namespace DiceLuck.Core.Contracts
{
    public interface ICasinoState : IState<long>
    {
        string JackpotWinnerName { get; set; }

        int JackpotNumber { get; set; }

        Dictionary<int, double> IncomeTable { get; set; } 
    }
}
