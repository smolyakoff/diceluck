﻿using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core
{
    public class CalculateIncomeMessage : IPayloadContainer<DiceRollResultPayload>, IAsyncRequest<DiceRollIncomePayload>
    {
        public DiceRollResultPayload Payload { get; set; }
    }
}