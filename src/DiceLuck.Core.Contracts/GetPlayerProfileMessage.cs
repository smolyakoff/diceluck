﻿using System;
using DiceLuck.Framework.Contracts;
using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core.Contracts
{
    public class GetPlayerProfileMessage : 
        IPayloadContainer<KeyPayload<Guid>>, 
        IMetadataContainer<Metadata>,
        IAsyncRequest<PlayerProfilePayload>
    {
        public KeyPayload<Guid> Payload { get; set; }

        public Metadata Metadata { get; set; }
    }
}
