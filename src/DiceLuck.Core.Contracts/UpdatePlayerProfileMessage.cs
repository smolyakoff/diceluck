﻿using System;
using DiceLuck.Framework.Contracts;
using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core.Contracts
{
    public class UpdatePlayerProfileMessage : IPayloadContainer<PlayerProfilePayload>, IMetadataContainer<Metadata>, IAsyncRequest
    {
        public Metadata Metadata { get; set; }

        public PlayerProfilePayload Payload { get; set; }
    }
}
