using DiceLuck.Core.Contracts;
using DiceLuck.Framework.Contracts.Messaging;
using MediatR;

namespace DiceLuck.Core
{
    public class VerifyJackpotMessage : IPayloadContainer<JackpotVerificationPayload>, IAsyncRequest<JackpotVerificationResultPayload>
    {
        public JackpotVerificationPayload Payload { get; set; }
    }
}