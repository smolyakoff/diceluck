﻿using System.Threading.Tasks;
using DiceLuck.Framework.Contracts.Persistence;
using MediatR;

namespace DiceLuck.Client
{
    public class PersistenceHandler : 
        IAsyncRequestHandler<ReadObjectMessage, object>,
        IAsyncRequestHandler<WriteObjectMessage, Unit>
    {
        private readonly ApiClient _client;

        public PersistenceHandler(ApiClient client)
        {
            _client = client;
        }

        public Task<object> Handle(ReadObjectMessage message)
        {
            return _client.Send<ReadObjectMessage, object>(message);
        }

        public Task<Unit> Handle(WriteObjectMessage message)
        {
            return _client.Send<WriteObjectMessage, Unit>(message);
        }
    }
}
