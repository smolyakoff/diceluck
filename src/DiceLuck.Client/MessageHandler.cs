﻿using System;
using System.Threading.Tasks;
using DiceLuck.Core;
using DiceLuck.Core.Contracts;
using MediatR;

namespace DiceLuck.Client
{
    public class MessageHandler : IGame, ICasino, IPlayer
    {
        private readonly IMediator _mediator;
        private readonly ApiClient _client;

        private Game _game;
        private Casino _casino;

        public MessageHandler(IMediator mediator, ApiClient client)
        {
            _mediator = mediator;
            _client = client;
        }

        public Task<Unit> Handle(StartGameMessage message)
        {
            _game = _game ?? new Game(new MediatorStateHandle<Guid, GameState>(_mediator, new GameState()), _mediator);
            return _game.Handle(message);
        }

        public Task<DiceRollOutcomePayload> Handle(RollDiceMessage message)
        {
            return _game.Handle(message);
        } 

        public Task<PlayerProfilePayload> Handle(GetPlayerProfileMessage message)
        {
            return _client.Send<GetPlayerProfileMessage, PlayerProfilePayload>(message);
        }

        public Task<Unit> Handle(UpdatePlayerProfileMessage message)
        {
            return _client.Send<UpdatePlayerProfileMessage, Unit>(message);
        }

        public Task<DiceRollIncomePayload> Handle(CalculateIncomeMessage message)
        {
            _casino = _casino ?? new Casino(new MediatorStateHandle<long, CasinoState>(_mediator, new CasinoState()));
            return _casino.Handle(message);
        }

        public Task<JackpotVerificationResultPayload> Handle(VerifyJackpotMessage message)
        {
            return _client.Send<VerifyJackpotMessage, JackpotVerificationResultPayload>(message);
        }
    }
}
