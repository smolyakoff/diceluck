﻿using System.Collections.Generic;
using System.Linq;
using DiceLuck.Core.Contracts;

namespace DiceLuck.Client
{
    public class CasinoState : ICasinoState
    {
        public CasinoState()
        {
            IncomeTable = Enumerable.Range(2, 12).ToDictionary(x => x, x => (double) x*5);
        }

        public long Key => default(long);

        public string JackpotWinnerName { get; set; }

        public int JackpotNumber { get; set; }

        public Dictionary<int, double> IncomeTable { get; set; }
    }
}
