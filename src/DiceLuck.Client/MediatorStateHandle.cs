﻿using System.Threading.Tasks;
using DiceLuck.Framework.Contracts.Persistence;
using MediatR;

namespace DiceLuck.Client
{
    public class MediatorStateHandle<TKey, TState> : IStateHandle<TKey, TState> where TState : class, IState<TKey>, new()
    {
        private readonly IMediator _mediator;
        private TState _state;
        private readonly object _key;

        public MediatorStateHandle(IMediator mediator, TState initialState) : this(mediator, initialState.Key)
        {
            _mediator = mediator;
            _state = initialState;
        }

        public MediatorStateHandle(IMediator mediator, object key)
        {
            _key = key;
            _state = null;
            _mediator = mediator;
        }

        public TState State
        {
            get
            {
                if (_state != null)
                {
                    return _state;
                }
                Restore().Wait();
                _state = _state ?? new TState();
                return _state;
            }
        }

        public Task Clear()
        {
            var message = new DeleteObjectMessage
            {
                Payload =
                {
                    Key = _state.Key,
                    Type = typeof (TState).FullName
                }
            };
            return _mediator.SendAsync(message);
        }

        public async Task Restore()
        {
            var message = new ReadObjectMessage
            {
                Payload =
                {
                    Key = _key,
                    Type = typeof (TState).FullName
                }
            };
            _state =  (TState) (await _mediator.SendAsync(message));
        }

        public Task Persist()
        {
            var message = new WriteObjectMessage
            {
                Payload =
                {
                    Key = _key,
                    Type = typeof (TState).FullName,
                    Value = _state
                }
            };
            return _mediator.SendAsync(message);
        }
    }
}
