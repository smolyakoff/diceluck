﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Autofac.Features.Variance;
using MediatR;

namespace DiceLuck.Client
{
    public class ClientModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            base.Load(builder);
            builder.RegisterSource(new ContravariantRegistrationSource());
            builder.RegisterInstance(Console.Out).As<TextWriter>();
            builder.RegisterType<Mediator>().As<IMediator>().SingleInstance();
            builder.RegisterType<MessageHandler>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PersistenceHandler>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterInstance(new ApiClientOptions());
            builder.RegisterType<ApiClient>().AsSelf().SingleInstance();

            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.Register<MultiInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
            });
        }
    }
}
