﻿using System;
using DiceLuck.Core;

namespace DiceLuck.Client
{
    public class GameState : IGameState
    {
        public GameState()
        {
            Key = Guid.NewGuid();
        }

        public Guid Key { get; }
        public int Rolls { get; set; }
        public double Income { get; set; }
    }
}
