﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using DiceLuck.Core.Contracts;
using DiceLuck.Framework.Contracts;
using DiceLuck.Framework.Contracts.Messaging;
using MediatR;
using Newtonsoft.Json;

namespace DiceLuck.Client
{
    public class ApiClient : IDisposable
    {
        private readonly ApiClientOptions _options;

        private readonly HttpClient _client;

        private readonly JsonMediaTypeFormatter _formatter;

        public ApiClient(ApiClientOptions options)
        {
            _options = options;
            _client = new HttpClient();
            _formatter = new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                }
            };
        }

        public async Task<TResponse> Send<TMessage, TResponse>(TMessage message) where TMessage : IAsyncRequest<TResponse>
        {
            var metaContainer = message as IMetadataContainer<Metadata>;
            if (metaContainer != null && metaContainer.Metadata == null)
            {
                metaContainer.Metadata = new Metadata {User = new UserMetadata {UserId = _options.UserId}};
            }

            var content = new ObjectContent<TMessage>(message, _formatter);
            var response = await _client.PostAsync($"{_options.Endpoint}/dispatch", content);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<TResponse>();
        }

        public Task Publish<TEvent>(TEvent @event)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }

    public class ApiClientOptions
    {
        public ApiClientOptions()
        {
            UserId = Guid.NewGuid();
            Endpoint = "http://localhost:9000/v1";
        }

        public Guid UserId { get; }

        public string Endpoint { get; }
    }
}
