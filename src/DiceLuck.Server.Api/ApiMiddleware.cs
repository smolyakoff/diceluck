﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Owin;
using MediatR;
using Microsoft.Owin;
using Newtonsoft.Json;

namespace DiceLuck.Server.Api
{
    public class ApiMiddleware : OwinMiddleware
    {
        private readonly JsonSerializer _serializer;

        public ApiMiddleware(OwinMiddleware next) : base(next)
        {
            _serializer = new JsonSerializer {TypeNameHandling = TypeNameHandling.All};
        }

        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                var scope = context.GetAutofacLifetimeScope();
                var mediator = scope.Resolve<IMediator>();
                object body;
                using (var streamReader = new StreamReader(context.Request.Body))
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    body = _serializer.Deserialize(jsonReader);
                }
                var reqInterfaces = body.GetType().GetInterfaces().ToList();
                var reqInterface = reqInterfaces
                    .Where(t => t.IsGenericType)
                    .First(t => t.GetGenericTypeDefinition() == typeof(IAsyncRequest<>));
                var responseType = reqInterface.GetGenericArguments().First();
                var methods = mediator.GetType().GetMethods()
                    .Where(m => m.Name == "SendAsync")
                    .ToList();
                var method = methods.First(m => m.GetParameters().Length == 1);
                var invoker = method.MakeGenericMethod(responseType);
                dynamic result = invoker.Invoke(mediator, new[] {body});
                dynamic response = await result;
                context.Response.ContentType = "application/json";
                using (var streamWriter = new StreamWriter(context.Response.Body))
                {
                    _serializer.Serialize(streamWriter, response);
                    await streamWriter.FlushAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }
    }
}
