﻿using Autofac;
using Owin;
using IContainer = Autofac.IContainer;

namespace DiceLuck.Server.Api.Host
{
    public class Startup
    {
        public void Configuration(IAppBuilder builder)
        {
            var container = CreateContainer();
            builder.UseAutofacMiddleware(container);
            builder.Use<ApiMiddleware>();
        }

        private static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new ApiModule());

            return builder.Build();
        }
    }
}