﻿using System;
using System.Linq;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Orleans;

namespace DiceLuck.Server.Silo.Host
{
    public class Startup
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<SingleInstanceFactory, SingleInstanceFactory>(p => (type) => p.GetService(type));
            services.AddScoped<MultiInstanceFactory, MultiInstanceFactory>(p => (type) => p.GetServices(type));
            services.AddSingleton<IMediator, Mediator>();
            var grains = typeof (CasinoGrain).Assembly.DefinedTypes
                .Where(t => t.IsClass)
                .Where(t => typeof (Grain).IsAssignableFrom(t))
                .ToList();
            foreach (var grain in grains)
            {
                var interfaces = grain.GetInterfaces()
                    .Where(t => typeof (IGrain).IsAssignableFrom(t))
                    .Where(t => t != typeof (IGrain))
                    .Where(t => !t.Name.EndsWith("Key"));
                services.AddTransient(grain, grain);
                foreach (var @interface in interfaces)
                {
                    services.AddTransient(@interface, grain);
                }
            }
            var provider = services.BuildServiceProvider();
            return provider;
        }
    }
}